import 'package:flutter/material.dart';

class ShipCard extends StatelessWidget {
  const ShipCard({super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
      child: ExpansionTile(
          children: [
            Padding(
              padding: EdgeInsets.all(8.0),
              child: TextFormField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(), hintText: "Digite seu Cep"),
                initialValue: "",
                onFieldSubmitted: (text) {},
              ),
            )
          ],
          leading: Icon(Icons.location_on),
          title: Text(
            "Calcular Frete",
            textAlign: TextAlign.start,
            style:
                TextStyle(fontWeight: FontWeight.w500, color: Colors.grey[700]),
          )),
    );
  }
}

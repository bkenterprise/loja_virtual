import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:loja_virtual/tiles/place_tile.dart';

class PlacesTab extends StatelessWidget {
  const PlacesTab({super.key});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<QuerySnapshot>(
      builder: (context, snapshot) {
        if (!snapshot.hasData)
          return Center(child: CircularProgressIndicator());
        else {
          return ListView(
              children: snapshot.data!.docs
                  .map((d) => PlaceTile(snapshot: d))
                  .toList());
        }
      },
      future: FirebaseFirestore.instance.collection("places").get(),
    );
  }
}

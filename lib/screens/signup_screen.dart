import 'package:flutter/material.dart';
import 'package:loja_virtual/models/user_model.dart';
import 'package:scoped_model/scoped_model.dart';

class SignUpScreen extends StatefulWidget {


  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final GlobalKey<FormState> _formKey = new GlobalKey();
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passController = TextEditingController();
  final _addressController = TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text("Criar Conta"),
          centerTitle: true,
        ),
        body: ScopedModelDescendant<UserModel>(
          builder: (context, child, model) {
            return model.isLoading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Form(
                    key: _formKey,
                    child: ListView(
                      padding: EdgeInsets.all(16.0),
                      children: [
                        TextFormField(
                          controller: _nameController,
                          validator: (nome) {
                            if (nome == null || nome.isEmpty)
                              return "Nome inválido";
                            return null;
                          },
                          decoration: InputDecoration(
                            hintText: "Nome Completo",
                          ),
                          keyboardType: TextInputType.emailAddress,
                        ),
                        SizedBox(
                          height: 16.0,
                        ),
                        TextFormField(
                          controller: _emailController,
                          validator: (email) {
                            RegExp reg = new RegExp(
                                r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
                            if (email != null &&
                                (email.isEmpty || !reg.hasMatch(email)))
                              return "Email inválido";
                            return null;
                          },
                          decoration: InputDecoration(
                            hintText: "E-mail",
                          ),
                        ),
                        SizedBox(
                          height: 16.0,
                        ),
                        TextFormField(
                          controller: _passController,
                          validator: (senha) {
                            if (senha != null &&
                                (senha.isEmpty || senha.length < 6))
                              return "Senha inválida";
                            return null;
                          },
                          decoration: InputDecoration(
                            hintText: "Senha",
                          ),
                          obscureText: true,
                        ),
                        SizedBox(
                          height: 16.0,
                        ),
                        TextFormField(
                          controller: _addressController,
                          validator: (endereco) {
                            if (endereco == null || endereco.isEmpty)
                              return "Endereço inválido";
                            return null;
                          },
                          decoration: InputDecoration(
                            hintText: "Endereço",
                          ),
                        ),
                        SizedBox(
                          height: 16.0,
                        ),
                        SizedBox(
                          height: 44.0,
                          child: ElevatedButton(
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {
                                Map<String, dynamic> userData = {
                                  'name': _nameController.text,
                                  'email': _emailController.text,
                                  'address': _addressController.text
                                };
                                model.signUp(
                                    userData: userData,
                                    pass: _passController.text,
                                    onSuccess: _onSuccess,
                                    onFail: _onFail);
                              }
                            },
                            style: ButtonStyle(
                                backgroundColor: MaterialStatePropertyAll(
                                    Theme.of(context).primaryColor)),
                            child: Text(
                              "Criar Conta",
                              style: TextStyle(
                                  fontSize: 18.0, color: Colors.white),
                            ),
                          ),
                        )
                      ],
                    ),
                  );
          },
        ));
  }

  void _onSuccess() {
    SnackBar snack = SnackBar(
      content: Text('Usuário criado com sucesso!'),
      backgroundColor: Theme.of(context).primaryColor,
      duration: Duration(seconds: 2),
    );

    ScaffoldMessenger.of(context).showSnackBar(snack);
    Future.delayed(Duration(seconds: 2))
        .then((value) => Navigator.of(context).pop());
  }

  void _onFail() {
    SnackBar snack = SnackBar(
      content: Text('Falha ao criar usuário!'),
      backgroundColor: Colors.red,
      duration: Duration(seconds: 2),
    );
    ScaffoldMessenger.of(context).showSnackBar(snack);
  }
}
